using System.Collections.Generic;

namespace TwoFacedBull
{
    partial class ApplianceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        // mapping of RFID tags to market
        private Dictionary<string, string> markets = new Dictionary<string, string>();
        private SpreadSheet spreadsheet = new SpreadSheet();
        private double currentPrice = 0.0;
        private bool hasGain = false;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.servo1 = new GroupLab.SharedPhidgets.Servo(this.components);
            this.servoSkin1 = new GroupLab.SharedPhidgets.ServoSkin();
            this.leftRFID = new GroupLab.SharedPhidgets.RFID(this.components);
            this.interfaceKit1 = new GroupLab.SharedPhidgets.InterfaceKit(this.components);
            this.interfaceKitSkin1 = new GroupLab.SharedPhidgets.InterfaceKitSkin();
            this.rightRFIDSkin = new GroupLab.SharedPhidgets.RFIDSkin();
            this.rightRFID = new GroupLab.SharedPhidgets.RFID(this.components);
            this.leftRFIDSkin = new GroupLab.SharedPhidgets.RFIDSkin();
            this.rfidGroupBox = new GroupLab.SharedPhidgets.Utilities.ExpandableGroupBox(this.components);
            this.sensorSkin1 = new GroupLab.SharedPhidgets.SensorSkin();
            this.leftRFIDSkin.SuspendLayout();
            this.SuspendLayout();
            // 
            // servo1
            // 
            this.servo1.AutoAttach = true;
            this.servo1.ComponentDescriptionPrefix = null;
            this.servo1.GUID = "70c5adc1-5be9-49c7-b9ea-be0a48dfcf25";
            this.servo1.Invisible = false;
            this.servo1.OverrideDesignTimeLock = false;
            this.servo1.PhidgetDevice = null;
            // 
            // servoSkin1
            // 
            this.servoSkin1.Location = new System.Drawing.Point(12, 46);
            this.servoSkin1.Name = "servoSkin1";
            this.servoSkin1.Servo = this.servo1;
            this.servoSkin1.Size = new System.Drawing.Size(963, 165);
            this.servoSkin1.SynchronousTrackbars = true;
            this.servoSkin1.TabIndex = 2;
            // 
            // leftRFID
            // 
            this.leftRFID.Antenna = false;
            this.leftRFID.AutoAttach = true;
            this.leftRFID.ComponentDescriptionPrefix = null;
            this.leftRFID.DelayTime = 1F;
            this.leftRFID.ExternalLED = false;
            this.leftRFID.ExternalOutput = false;
            this.leftRFID.FilterSerialNumbers.Add("16212");
            this.leftRFID.GUID = "3afd9bad-1e0a-4e7d-9849-a2a8a13c7ce2";
            this.leftRFID.Invisible = false;
            this.leftRFID.LED = false;
            this.leftRFID.OverrideDesignTimeLock = false;
            this.leftRFID.PhidgetDevice = null;
            this.leftRFID.UseDelayTime = false;
            // 
            // interfaceKit1
            // 
            this.interfaceKit1.AutoAttach = true;
            this.interfaceKit1.ComponentDescriptionPrefix = null;
            this.interfaceKit1.GUID = "04a77cb8-cb3b-4669-81e8-b67da61f2c89";
            this.interfaceKit1.Invisible = false;
            this.interfaceKit1.OverrideDesignTimeLock = false;
            this.interfaceKit1.PhidgetDevice = null;
            this.interfaceKit1.SensorChange += new GroupLab.SharedPhidgets.IndexSensorChangeEventHandler(this.interfaceKit1_SensorChange);
            // 
            // interfaceKitSkin1
            // 
            this.interfaceKitSkin1.InterfaceKit = this.interfaceKit1;
            this.interfaceKitSkin1.Location = new System.Drawing.Point(12, 205);
            this.interfaceKitSkin1.Name = "interfaceKitSkin1";
            this.interfaceKitSkin1.Size = new System.Drawing.Size(487, 261);
            this.interfaceKitSkin1.TabIndex = 3;
            // 
            // rightRFIDSkin
            // 
            this.rightRFIDSkin.Location = new System.Drawing.Point(506, 486);
            this.rightRFIDSkin.Name = "rightRFIDSkin";
            this.rightRFIDSkin.RFID = this.rightRFID;
            this.rightRFIDSkin.Size = new System.Drawing.Size(459, 242);
            this.rightRFIDSkin.TabIndex = 5;
            // 
            // rightRFID
            // 
            this.rightRFID.Antenna = false;
            this.rightRFID.AutoAttach = true;
            this.rightRFID.ComponentDescriptionPrefix = null;
            this.rightRFID.DelayTime = 1F;
            this.rightRFID.ExternalLED = false;
            this.rightRFID.ExternalOutput = false;
            this.rightRFID.FilterSerialNumbers.Add("6852");
            this.rightRFID.GUID = "08cc1be1-1cf3-4f23-aa41-5c622fd8b81f";
            this.rightRFID.Invisible = false;
            this.rightRFID.LED = false;
            this.rightRFID.OverrideDesignTimeLock = false;
            this.rightRFID.PhidgetDevice = null;
            this.rightRFID.UseDelayTime = false;
            // 
            // leftRFIDSkin
            // 
            this.leftRFIDSkin.Controls.Add(this.rfidGroupBox);
            this.leftRFIDSkin.Location = new System.Drawing.Point(506, 233);
            this.leftRFIDSkin.Name = "leftRFIDSkin";
            this.leftRFIDSkin.RFID = this.leftRFID;
            this.leftRFIDSkin.Size = new System.Drawing.Size(459, 233);
            this.leftRFIDSkin.TabIndex = 6;
            // 
            // rfidGroupBox
            // 
            this.rfidGroupBox.ControlParentHeight = false;
            this.rfidGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rfidGroupBox.Location = new System.Drawing.Point(0, 0);
            this.rfidGroupBox.Name = "rfidGroupBox";
            this.rfidGroupBox.ResizeMode = false;
            this.rfidGroupBox.ResizeModeParent = false;
            this.rfidGroupBox.SetContextMenu = null;
            this.rfidGroupBox.Size = new System.Drawing.Size(459, 233);
            this.rfidGroupBox.TabIndex = 4;
            this.rfidGroupBox.TabStop = false;
            this.rfidGroupBox.Text = "RFID - Not Attached";
            this.rfidGroupBox.UseCollapseMode = true;
            this.rfidGroupBox.UseMenuMode = false;
            // 
            // sensorSkin1
            // 
            this.sensorSkin1.InterfaceKit = this.interfaceKit1;
            this.sensorSkin1.LineAndGraphColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.sensorSkin1.Location = new System.Drawing.Point(12, 472);
            this.sensorSkin1.Name = "sensorSkin1";
            this.sensorSkin1.NamePrefix = null;
            this.sensorSkin1.SensorNumber = 0;
            this.sensorSkin1.Size = new System.Drawing.Size(487, 256);
            this.sensorSkin1.TabIndex = 7;
            // 
            // ApplianceForm
            // 
            this.ApplianceName = "Appliance";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 785);
            this.Controls.Add(this.sensorSkin1);
            this.Controls.Add(this.leftRFIDSkin);
            this.Controls.Add(this.rightRFIDSkin);
            this.Controls.Add(this.interfaceKitSkin1);
            this.Controls.Add(this.servoSkin1);
            this.Name = "ApplianceForm";
            this.Text = "Shared Phidgets Appliance";
            this.Controls.SetChildIndex(this.servoSkin1, 0);
            this.Controls.SetChildIndex(this.interfaceKitSkin1, 0);
            this.Controls.SetChildIndex(this.rightRFIDSkin, 0);
            this.Controls.SetChildIndex(this.leftRFIDSkin, 0);
            this.Controls.SetChildIndex(this.sensorSkin1, 0);
            this.leftRFIDSkin.ResumeLayout(false);
            this.ResumeLayout(false);

            setup();
        }

        #endregion

        public void setup()
        {
            markets.Add("0102ac3800", "TSX" );
            markets.Add("0102ac3f20", "DOW");
            markets.Add("0102ac16d9", "NASDAQ");
            markets.Add("0102ac3f9a", "SP500");
            markets.Add("0102ac1a0d", "NYSE");
            markets.Add("0102ac4019", "FTSE100");
            markets.Add("0102ac3d1b", "NIKKEI225");
            markets.Add("0102ac3ac9", "HANGSENG");

            if (interfaceKit1.Attached)
            {
                interfaceKit1.SensorChange += new GroupLab.SharedPhidgets.IndexSensorChangeEventHandler(interfaceKit1_SensorChange);
            }
             
            rightRFID.Tag += new GroupLab.SharedPhidgets.RFIDTagEventHandler(rightRFID_Tag);
            leftRFID.Tag += new GroupLab.SharedPhidgets.RFIDTagEventHandler(leftRFID_Tag);
        }

        void interfaceKit1_SensorChange(object sender, GroupLab.SharedPhidgets.IndexSensorChangeEventArgs e)
        {

            if (e.Value < 60)
            {
                rightRFID.Antenna = false;
                leftRFID.Antenna = true;
            }

            else
            {
                rightRFID.Antenna = true;
                leftRFID.Antenna = false;
            }
        }

        void leftRFID_Tag(object sender, GroupLab.SharedPhidgets.RFIDTagEventArgs e)
        {
            if(e.Tag.Equals("01024c8080"))
            {
                servo1.Motors[0].Position = 134;
                servo1.Motors[1].Position = 115;
                servo1.Motors[2].Position = 160;
                servo1.Motors[3].Position = 54;

                for (int i = 0; i < interfaceKit1.Outputs.Count; i++)
                {
                    interfaceKit1.Outputs[i].State = false;
                }
            }

            if (markets.ContainsKey(e.Tag))
            {
                string market = markets[e.Tag];
                currentPrice = spreadsheet.getPriceByName(market);

                if (spreadsheet.getChangeByName(market) < 0)
                {
                    interfaceKit1.Outputs[4].State = true;
                    interfaceKit1.Outputs[6].State = false;
                    servo1.Motors[2].Position = 180;
                    servo1.Motors[3].Position = 5;
                }
                else
                {
                    interfaceKit1.Outputs[4].State = false;
                    interfaceKit1.Outputs[6].State = true;
                    servo1.Motors[2].Position = 180;
                    servo1.Motors[3].Position = 86;
                }
            }
        }

        void rightRFID_Tag(object sender, GroupLab.SharedPhidgets.RFIDTagEventArgs e)
        {
            //saul
            if (e.Tag.Equals("01024a913e"))
            {
                    interfaceKit1.Outputs[2].State = false;
                    interfaceKit1.Outputs[3].State = true;
                    servo1.Motors[0].Position = 173;
                    servo1.Motors[1].Position = 54;

                    interfaceKit1.Outputs[4].State = true;
                    interfaceKit1.Outputs[6].State = false;
                    servo1.Motors[2].Position = 180;
                    servo1.Motors[3].Position = 5;
            }

            //reset
            if (e.Tag.Equals("01024c8080"))
            {
                servo1.Motors[0].Position = 134;
                servo1.Motors[1].Position = 115;
                servo1.Motors[2].Position = 128;
                servo1.Motors[3].Position = 83;

                for (int i = 0; i < interfaceKit1.Outputs.Count; i++ )
                {
                    interfaceKit1.Outputs[i].State = false;
                }
            }

            if (markets.ContainsKey(e.Tag))
            {
                string market = markets[e.Tag];
                currentPrice = spreadsheet.getPriceByName(market);

                if (spreadsheet.getChangeByName(market) < 0)
                {
                    interfaceKit1.Outputs[2].State = false;
                    interfaceKit1.Outputs[3].State = true;
                    servo1.Motors[0].Position = 173;
                    servo1.Motors[1].Position = 54;
                }
                else
                {
                    interfaceKit1.Outputs[2].State = true;
                    interfaceKit1.Outputs[3].State = false;
                    servo1.Motors[0].Position = 101;
                    servo1.Motors[1].Position = 139;
                }
            }
        }

        private GroupLab.SharedPhidgets.Servo servo1;
        private GroupLab.SharedPhidgets.ServoSkin servoSkin1;
        private GroupLab.SharedPhidgets.RFID leftRFID;
        private GroupLab.SharedPhidgets.InterfaceKit interfaceKit1;
        private GroupLab.SharedPhidgets.InterfaceKitSkin interfaceKitSkin1;
        private GroupLab.SharedPhidgets.RFIDSkin rightRFIDSkin;
        private GroupLab.SharedPhidgets.RFIDSkin leftRFIDSkin;
        private GroupLab.SharedPhidgets.Utilities.ExpandableGroupBox rfidGroupBox;
        private GroupLab.SharedPhidgets.RFID rightRFID;
        private GroupLab.SharedPhidgets.SensorSkin sensorSkin1;

    }
}

