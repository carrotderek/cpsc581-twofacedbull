// ----------------------------------------------------------------------------
// SHARED PHIDGETS APPLIANCE IMPLEMENTATION
// ---------------------------------------------------------------------------- 
// Author:      derek
// Filename:    ApplianceForm.cs
// Version:     1.0
// Revision:    $Revision: 747 $ 
// Modified:    $Date: $
// ----------------------------------------------------------------------------
// Copyright 2009 derek
// ----------------------------------------------------------------------------
// SUBVERSION: $Id: $
// ----------------------------------------------------------------------------
// 
// Appliance Implementation:
// This appliance implementation template can be used to build
// information appliances with distributed physical hardware devices.
// The developer library of the Shared Phidgets toolkit provides
// the API and building blocks to integrate diverse physical hardware
// devices into custom applications.
//
// ----------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GroupLab.Networking;
using GroupLab.SharedPhidgets;
using GroupLab.SharedPhidgets.Appliances;
using GroupLab.SharedPhidgets.Simulation;
using GroupLab.SharedPhidgets.Utilities;


namespace TwoFacedBull
{
    /// <summary>
    /// Shared Phidgets Appliance:
    /// Template for the implementation of appliances that build
    /// distributed physical user interfaces with the Shared Phidgets 
    /// toolkit.
    /// </summary>
    public partial class ApplianceForm : Appliance
    {
        #region Members

        /// <summary>
        /// The instance of the shared dictionary; can be utilised for direct
        /// access to the shared data model.
        /// </summary>
        private SharedDictionary sharedDictionary;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor of the main user interface form
        /// </summary>
        public ApplianceForm()
        {
            // Initialise the components of the GUI
            InitializeComponent();
            
            // Start feed from Google Spreadsheets
            SpreadSheet s = new SpreadSheet();

            // Set the instance of the shared dictionary
            this.sharedDictionary = ConnectionSingleton.Instance.GetSharedDictionary();
        }

        #endregion

    }
}