﻿using System;
using System.Collections;
using System.Text;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;

namespace TwoFacedBull
{
    class SpreadSheet
    {
        private static string username, password;
        private static ArrayList allWorkSheets = new ArrayList();
        private static SpreadsheetsService service;
        private AtomLink listFeedLink;
        private ListQuery listQuery;
        private ListFeed listFeed;

        public SpreadSheet()
        {
            service = new SpreadsheetsService("stock-market");
            service.setUserCredentials("shurai", "$gva99void!");

            SpreadsheetQuery query = new SpreadsheetQuery();
            SpreadsheetFeed feed = service.Query(query);

            SpreadsheetEntry ssentry = (SpreadsheetEntry)feed.Entries[0];

            AtomLink sslink = ssentry.Links.FindService(GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery wkquery = new WorksheetQuery(sslink.HRef.ToString());
            WorksheetFeed wkfeed = service.Query(wkquery);
            WorksheetEntry wkentry = (WorksheetEntry)wkfeed.Entries[0];

            listFeedLink = wkentry.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);
            listQuery = new ListQuery(listFeedLink.HRef.ToString());
            listFeed = service.Query(listQuery);

            Console.WriteLine("Worksheet has {0} rows: ", listFeed.Entries.Count);
            foreach (ListEntry worksheetRow in listFeed.Entries)
            {
                ListEntry.CustomElementCollection elements = worksheetRow.Elements;
                foreach (ListEntry.Custom element in elements)
                {
                    Console.Write(element.Value + "\t");
                }
                Console.WriteLine();
            }
        }

        public double getPriceByName(String name)
        {
            ListQuery query = new ListQuery(listFeedLink.HRef.ToString());
            query.SpreadsheetQuery = "market=" + name;
            listFeed = service.Query(query);

            ListEntry e = (ListEntry)listFeed.Entries[0];
            string num = e.Elements[1].Value.ToString();
            double result = Convert.ToDouble(num);
            Console.WriteLine(result);

            return result;
        }

        public double getChangeByName(String name)
        {
            ListQuery query = new ListQuery(listFeedLink.HRef.ToString());
            query.SpreadsheetQuery = "market=" + name;
            listFeed = service.Query(query);

            ListEntry e = (ListEntry)listFeed.Entries[0];
            string num = e.Elements[2].Value.ToString();
            double result = Convert.ToDouble(num);
            Console.WriteLine(result);

            return result;
        }
    }
}
