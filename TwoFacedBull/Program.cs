// ----------------------------------------------------------------------------
// SHARED PHIDGETS APPLIANCE IMPLEMENTATION
// ---------------------------------------------------------------------------- 
// Author:      derek
// Filename:    Program.cs
// Version:     1.0
// Revision:    $Revision: 747 $ 
// Modified:    $Date: $
// ----------------------------------------------------------------------------
// Copyright 2009 derek
// ----------------------------------------------------------------------------
// SUBVERSION: $Id: $
// ----------------------------------------------------------------------------
// 
// Appliance Implementation:
// The main entry point for the application. Starts a new instance
// of the SharedPhidgetsUI form.
//
// ----------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace TwoFacedBull
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ApplianceForm());
        }
    }
}